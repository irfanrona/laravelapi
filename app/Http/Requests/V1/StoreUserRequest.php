<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();
        
        return $user != null && $user->tokenCan('user:create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'departmentId' => ['required','integer'],
            'subdepartmentId' => ['required','integer'],
            'roleId' => ['required','integer'],
            'name' => ['required'],
            'email' => ['required','email','unique:users'],
            'password' => ['required'],
            'jobTitle' => ['required'],
            'profilePicture' => ['image','mimes:jpg,jpeg,png','max:2048'],

        ];
    }

    protected function prepareForValidation(){
        $this->merge([
            'department_id' => $this->departmentId,
            'subdepartment_id' => $this->subdepartmentId,
            'role_id' => $this->roleId,
            'job_title' => $this->jobTitle,
            // 'profile_picture' =>$this->profilePicture,
            'password' => $this->password ? Hash::make($this->password) : null
        ]);
    }
}
