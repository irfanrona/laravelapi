<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();
        
        return $user != null && $user->tokenCan('activity:create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'outputId' => ['required','integer'],
            'name' => ['required'],
            'from' => ['required', Rule::in(['Q1','Q2','Q3','Q4'])],
            'to' => ['required', Rule::in(['Q1','Q2','Q3','Q4'])],
            'status' => ['required', Rule::in(['progress', 'delay', 'finish'])],
            'participantMale' => ['required','integer'],
            'participantFemale' => ['required','integer'],
            'time' => ['nullable'],
            'result' => ['nullable']
        ];
    }

    protected function prepareForValidation(){
        $this->merge([
            'output_id' => $this->outputId,
            'participant_male' => $this->participantMale,
            'participant_female' => $this->participantFemale
        ]);
    }
}
