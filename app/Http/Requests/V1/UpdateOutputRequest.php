<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateOutputRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();

        return $user != null && $user->tokenCan('output:update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();

        if($method == 'PUT'){
            return [
                'name' => ['required'],
                'departmentId' => ['required','integer'],
                'subdepartmentId' => ['required','integer'],
                'percentage' => ['required','integer'],
                'location' => ['required'],
                'challange' => ['nullable'],
                'followUp' => ['nullable'],
                'linkReport' => ['nullable'],
                'sdgs' => ['nullable'],
                'linkPhoto1' => ['image','mimes:jpg,jpeg,png','max:2048'],
                'linkPhoto2' => ['image','mimes:jpg,jpeg,png','max:2048'],
                'linkPhoto3' => ['image','mimes:jpg,jpeg,png','max:2048'],
                'linkPhoto4' => ['image','mimes:jpg,jpeg,png','max:2048'],
                'descPhoto1' => ['nullable'],
                'descPhoto2' => ['nullable'],
                'descPhoto3' => ['nullable'],
                'descPhoto4' => ['nullable'],
                'createdBy' => ['required','integer'],
            ];
        }else{
            return [
                'name' => ['sometimes','required'],
                'departmentId' => ['sometimes','required','integer'],
                'subdepartmentId' => ['sometimes','required','integer'],
                'percentage' => ['sometimes','required','integer'],
                'location' => ['sometimes','required'],
                'challange' => ['sometimes','nullable'],
                'followUp' => ['sometimes','nullable'],
                'linkReport' => ['sometimes','nullable'],
                'sdgs' => ['sometimes','nullable'],
                'linkPhoto1' => ['sometimes','image','mimes:jpg,jpeg,png','max:2048'],
                'linkPhoto2' => ['sometimes','image','mimes:jpg,jpeg,png','max:2048'],
                'linkPhoto3' => ['sometimes','image','mimes:jpg,jpeg,png','max:2048'],
                'linkPhoto4' => ['sometimes','image','mimes:jpg,jpeg,png','max:2048'],
                'descPhoto1' => ['sometimes','nullable'],
                'descPhoto2' => ['sometimes','nullable'],
                'descPhoto3' => ['sometimes','nullable'],
                'descPhoto4' => ['sometimes','nullable'],
                'createdBy' => ['sometimes','required','integer'],
            ];
        }
    }

    protected function prepareForValidation(){
        if ($this->followUp) {
            $this->merge([
                'follow_up' => $this->followUp
            ]);
        }
        if ($this->linkReport) {
            $this->merge([
                'link_report' => $this->linkReport
            ]);
        }
        if ($this->linkPhoto1) {
            $this->merge([
                'link_photo_1' => $this->linkPhoto1
            ]);
        }
        if ($this->linkPhoto2) {
            $this->merge([
                'link_photo_2' => $this->linkPhoto2
            ]);
        }
        if ($this->linkPhoto3) {
            $this->merge([
                'link_photo_3' => $this->linkPhoto3
            ]);
        }
        if ($this->linkPhoto4) {
            $this->merge([
                'link_photo_4' => $this->linkPhoto4
            ]);
        }
        if ($this->descPhoto1) {
            $this->merge([
                'desc_photo_1' => $this->descPhoto1
            ]);
        }
        if ($this->descPhoto2) {
            $this->merge([
                'desc_photo_2' => $this->descPhoto2
            ]);
        }
        if ($this->descPhoto3) {
            $this->merge([
                'desc_photo_3' => $this->descPhoto3
            ]);
        }
        if ($this->descPhoto4) {
            $this->merge([
                'desc_photo_4' => $this->descPhoto4
            ]);
        }
    }
}
