<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();

        return $user != null && $user->tokenCan('user:update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();

        if($method == 'PUT'){
            return [
                'departmentId' => ['required','integer'],
                'subdepartmentId' => ['required','integer'],
                'name' => ['required'],
                'email' => ['required','email'],
                'jobTitle' => ['required'],
                'profilePicture' => ['image','mimes:jpg,jpeg,png','max:2048'],
                'password' => ['min:4'],
            ];
        }else{
            return [
                'departmentId' => ['sometimes','required','integer'],
                'subdepartmentId' => ['sometimes','required','integer'],
                'name' => ['sometimes','required'],
                'email' => ['sometimes','required','email'],
                'jobTitle' => ['sometimes','required'],
                'profilePicture' => ['sometimes','image','mimes:jpg,jpeg,png','max:2048'],
                'password' => ['sometimes','min:4'],
            ];
        }
    }

    protected function prepareForValidation(){
        if ($this->departmentId) {
            $this->merge([
                'department_id' => $this->departmentId
            ]);
        }
        if ($this->subdepartmentId) {
            $this->merge([
                'subdepartment_id' => $this->subdepartmentId
            ]);
        }
        if ($this->roleId) {
            $this->merge([
                'role_id' => $this->roleId
            ]);
        }
        if ($this->jobTitle) {
            $this->merge([
                'job_title' => $this->jobTitle
            ]);
        }
        if ($this->password) {
            $this->merge([
                'password' => Hash::make($this->password)
            ]);
        }
    }
}
