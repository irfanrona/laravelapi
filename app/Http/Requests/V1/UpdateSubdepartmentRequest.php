<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSubdepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();

        return $user != null && $user->tokenCan('subdepartment:update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();

        if($method == 'PUT'){
            return [
                'departmentId' => ['required','integer'],
                'name' => ['required'],
                'logoPicture' => ['image','mimes:jpg,jpeg,png','max:2048']
            ];
        }else{
            return [
                'departmentId' => ['sometimes','required','integer'],
                'name' => ['sometimes','required'],
                'logoPicture' => ['sometimes','image','mimes:jpg,jpeg,png','max:2048']
            ];
        }
    }

    protected function prepareForValidation(){
        if ($this->departmentId) {
            $this->merge([
                'department_id' => $this->departmentId
            ]);
        }
    }
}
