<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSubdepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();
        
        return $user != null && $user->tokenCan('subdepartment:create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'departmentId' => ['required','integer'],
            'name' => ['required'],
            'logoPicture' => ['image','mimes:jpg,jpeg,png','max:2048'],
        ];
    }

    protected function prepareForValidation(){
        $this->merge([
            'department_id' => $this->departmentId
        ]);
    }
}
