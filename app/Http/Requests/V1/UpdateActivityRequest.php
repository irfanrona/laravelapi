<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();

        return $user != null && $user->tokenCan('activity:update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->method();

        if($method == 'PUT'){
            return [
                'outputId' => ['required','integer'],
                'name' => ['required'],
                'from' => ['required', Rule::in(['Q1','Q2','Q3','Q4'])],
                'to' => ['required', Rule::in(['Q1','Q2','Q3','Q4'])],
                'status' => ['required', Rule::in(['progress', 'delay', 'finish'])],
                'participantMale' => ['required','integer'],
                'participantFemale' => ['required','integer'],
                'time' => ['nullable'],
                'result' => ['nullable']
            ];
        }else{
            return [
                'outputId' => ['sometimes','required','integer'],
                'name' => ['sometimes','required'],
                'from' => ['sometimes','required', Rule::in(['Q1','Q2','Q3','Q4'])],
                'to' => ['sometimes','required', Rule::in(['Q1','Q2','Q3','Q4'])],
                'status' => ['sometimes','required', Rule::in(['progress', 'delay', 'finish'])],
                'participantMale' => ['sometimes','required','integer'],
                'participantFemale' => ['sometimes','required','integer'],
                'time' => ['sometimes','nullable'],
                'result' => ['sometimes','nullable']
            ];
        }
    }

    protected function prepareForValidation(){
        if ($this->outputId) {
            $this->merge([
                'output_id' => $this->outputId
            ]);
        }
        if ($this->participantMale) {
            $this->merge([
                'participant_male' => $this->participantMale
            ]);
        }
        if ($this->participantFemale) {
            $this->merge([
                'participant_female' => $this->participantFemale
            ]);
        }
    }
}
