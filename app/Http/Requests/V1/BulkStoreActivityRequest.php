<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BulkStoreActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();
        
        return $user != null && $user->tokenCan('activity:create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.outputId' => ['required','integer'],
            '*.name' => ['required'],
            '*.from' => ['required', Rule::in(['Q1','Q2','Q3','Q4'])],
            '*.to' => ['required', Rule::in(['Q1','Q2','Q3','Q4'])],
            '*.status' => ['required', Rule::in(['progress', 'delay', 'finish'])],
            '*.participantMale' => ['required','number'],
            '*.participanFemale' => ['required','number'],
            '*.time' => ['nullable'],
            '*.result' => ['nullable']
        ];
    }

    protected function prepareForValidation(){
        $data = [];

        foreach ($this->toArray() as $obj) {
            $obj['output_id'] = $obj['outputId'] ?? null;
            $obj['participant_male'] = $obj['participantMale'] ?? null;
            $obj['participant_female'] = $obj['participantFemale'] ?? null;

            $data[] = $obj;
        }
        $this->merge($data);
    }
}
