<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreOutputRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();
        
        return $user != null && $user->tokenCan('output:create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'departmentId' => ['required','integer'],
            'subdepartmentId' => ['required','integer'],
            'percentage' => ['required','integer'],
            'location' => ['required'],
            'challange' => ['nullable'],
            'followUp' => ['nullable'],
            'linkReport' => ['nullable'],
            'sdgs' => ['array'],
            'sdgs.*.id' => ['exists:sdgs,id'],
            'linkPhoto1' => ['image','mimes:jpg,jpeg,png','max:2048'],
            'linkPhoto2' => ['image','mimes:jpg,jpeg,png','max:2048'],
            'linkPhoto3' => ['image','mimes:jpg,jpeg,png','max:2048'],
            'linkPhoto4' => ['image','mimes:jpg,jpeg,png','max:2048'],
            'descPhoto1' => ['nullable'],
            'descPhoto2' => ['nullable'],
            'descPhoto3' => ['nullable'],
            'descPhoto4' => ['nullable'],
            'createdBy' => ['required','integer'],
        ];
    }

    protected function prepareForValidation(){
        $this->merge([
            'department_id' => $this->departmentId,
            'subdepartment_id' => $this->subdepartmentId,
            'follow_up' => $this->followUp,
            'link_report' => $this->linkReport,
            // 'link_photo_1' => $this->linkPhoto1,
            // 'link_photo_2' => $this->linkPhoto2,
            // 'link_photo_3' => $this->linkPhoto3,
            // 'link_photo_4' => $this->linkPhoto4,
            'desc_photo_1' => $this->descPhoto1,
            'desc_photo_2' => $this->descPhoto2,
            'desc_photo_3' => $this->descPhoto3,
            'desc_photo_4' => $this->descPhoto4,
            'created_by' => $this->createdBy
        ]);
    }
}
