<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use App\Models\Department;
use App\Models\Subdepartment;

class OutputResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'departmentId' => $this->when(!$request->query('includeDepartment'), $this->department_id),
            'department' => $this->when($request->query('includeDepartment'), new DepartmentResource(Department::findOrFail($this->department_id))),
            'subdepartmentId' => $this->when(!$request->query('includeSubdepartment'), $this->subdepartment_id),
            'subdepartment' => $this->when($request->query('includeSubdepartment'), new SubdepartmentResource(Subdepartment::findOrFail($this->subdepartment_id))),
            'name' => $this->name,
            'percentage' => $this->percentage,
            'location' => $this->location,
            'challange' => $this->challange,
            'followUp' => $this->follow_up,
            'linkReport' => $this->link_report,
            'sdgs' => $this->sdgs,
            'linkPhoto1' => url('/') . Storage::url($this->link_photo_1),
            'linkPhoto2' => url('/') . Storage::url($this->link_photo_2),
            'linkPhoto3' => url('/') . Storage::url($this->link_photo_3),
            'linkPhoto4' => url('/') . Storage::url($this->link_photo_4),
            'descPhoto1' => $this->desc_photo_1,
            'descPhoto2' => $this->desc_photo_2,
            'descPhoto3' => $this->desc_photo_3,
            'descPhoto4' => $this->desc_photo_4,
            'activities' => ActivityResource::collection($this->whenLoaded('activities')),
            'createdBy' => $this->created_by,
        ];
    }

    
}
