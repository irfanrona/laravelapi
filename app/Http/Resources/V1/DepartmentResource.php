<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class DepartmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $alias = $this->generateDepartmentAlias($this->name);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'alias' => $alias,
            'logoPicture' => url('/') . Storage::url($this->logo_picture),
            'subdepartments' => SubdepartmentResource::collection($this->whenLoaded('subdepartments')),
        ];
    }

    function generateDepartmentAlias($departmentName)
    {
        // Custom logic to generate first letter from word based on the department name
        $words = explode(' ', $departmentName);
        $aliasWords = array_map(function ($word) {
            return strtoupper(substr($word, 0, 1));
        }, $words);

        // Check if the first word has less than 3 characters
        if (strlen($words[0]) < 3) {
            // Take the next letter from the second word
            $aliasWords[0] .= strtoupper(substr($words[1], 0, 1));
        }

        $alias = implode('', $aliasWords);

        return $alias;
    }
}
