<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use App\Models\Role;
use App\Models\Department;
use App\Models\Subdepartment;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'departmentId' => $this->when(!$request->query('includeDepartment'), $this->department_id),
            'department' => $this->when($request->query('includeDepartment'), new DepartmentResource(Department::findOrFail($this->department_id))),
            'subdepartmentId' => $this->when(!$request->query('includeSubdepartment'), $this->subdepartment_id),
            'subdepartment' => $this->when($request->query('includeSubdepartment'), new SubdepartmentResource(Subdepartment::findOrFail($this->subdepartment_id))),
            'roleId' => $this->when(!$request->query('includeRole'), $this->role_id),
            'role' => $this->when($request->query('includeRole'), new RoleResource(Role::findOrFail($this->role_id))),
            'name' => $this->name,
            'email' => $this->email,
            'jobTitle' => $this->job_title,
            'profilePicture' => url('/') . Storage::url($this->profile_picture),
        ];
    }
}
