<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\V1\StoreUserRequest;
use App\Http\Requests\V1\UpdateUserRequest;
use App\Http\Requests\V1\DeleteUserRequest;
use App\Http\Resources\V1\UserResource;
use App\Http\Resources\V1\UserCollection;
use Illuminate\Http\Request;
use App\Filters\V1\UsersFilter;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

//export
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = new UsersFilter();
        $filterItems = $filter->transform($request); // ['column', 'operator', 'value']

        if ($request->has('departmentId')) {
            $filterItems[] = ['department_id', '=', $request->input('departmentId')];
        }

        $users = User::where($filterItems);
        $perPage = $request->query('per_page', 10);

        return new UserCollection($users->paginate($perPage)->appends($request->query()));
    }

    public function me()
    {
        $user = auth()->user();

        if ($user) {
            return new UserResource($user);
        } else {
            return response()->json(['error' => 'Unauthenticated'], 401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $originalFileName = $request->file('profilePicture') ? $request->file('profilePicture')->getClientOriginalName() : 'default.jpg';
        $extension = $request->file('profilePicture') ? $request->file('profilePicture')->getClientOriginalExtension() : 'jpg';

        // Generate a random file name if profilePicture is not empty
        $randomFileName = $request->file('profilePicture') ? uniqid() . '.' . $extension : 'default.jpg';
        
        // Store the uploaded file with the random file name if profilePicture is not empty
        $path = $request->file('profilePicture') ? Storage::putFileAs('public/images/profiles', $request->file('profilePicture'), $randomFileName) : 'public/images/profiles/default.jpg';
        
        // Modify the values in the $request object
        $request->merge([
            'profile_picture' => $path
        ]);

        return new UserResource(User::create($request->all()));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        // return $request->all();
        if ($request->file('profilePicture')) {
            // Generate a random file name
            $randomFileName = uniqid() . '.' . $request->file('profilePicture')->getClientOriginalExtension();

            // Store the new profile picture file
            $path = $request->file('profilePicture')->storeAs('public/images/profiles', $randomFileName);

            // Delete the previous profile picture if it is not the default.jpg
            if ($user->profilePicture != 'public/images/profiles/default.jpg') {
                try {
                    Storage::disk('public')->delete($user->profilePicture); //not deleted
                } catch (\Throwable $e) {
                    return $e;
                }
                
                
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'profile_picture' => $path,
                ]);
            }else{
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'profile_picture' => 'public/images/profiles/default.jpg',
                ]);
            } 
        }
        
        if($user->update($request->all())){
            return [
                'message' => 'User has been updated'
            ];
        }else{
            return [
                'message' => 'Error while updating User'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteUserRequest $request, User $user)
    {
        if($user->delete($request->all())){
            return [
                'message' => 'User has been deleted'
            ];
        }else{
            return [
                'message' => 'Error while deleting User'
            ];
        }
    }

    public function export() 
    {
        return Excel::download(new UsersExport, 'users.pdf', \Maatwebsite\Excel\Excel::MPDF);
    }
}
