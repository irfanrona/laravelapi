<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Http\Requests\V1\StoreRoleRequest;
use App\Http\Requests\V1\UpdateRoleRequest;
use App\Http\Requests\V1\DeleteRoleRequest;
use App\Http\Resources\V1\RoleResource;
use App\Http\Resources\V1\RoleCollection;
use Illuminate\Http\Request;
use App\Filters\V1\RolesFilter;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = new RolesFilter();
        $filterItems = $filter->transform($request); // ['column', 'operator', 'value']

        $includeUsers = $request->query('includeUsers');

        $roles = Role::where($filterItems);

        if ($includeUsers) {
            $roles = $roles->with('users');
        }

        return new RoleCollection($roles->paginate()->appends($request->query()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRoleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        return new RoleResource(Role::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $includeUsers = request()->query('includeUsers');

        if ($includeUsers) {
            return new RoleResource($role->loadMissing('users'));
        }
        return new RoleResource($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRoleRequest  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        if($role->update($request->all())){
            return [
                'message' => 'Role has been updated'
            ];
        }else{
            return [
                'message' => 'Error while updating Role'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteRoleRequest $request, Role $role)
    {
        if($role->delete($request->all())){
            return [
                'message' => 'Role has been deleted'
            ];
        }else{
            return [
                'message' => 'Error while deleting Role'
            ];
        }
    }
}
