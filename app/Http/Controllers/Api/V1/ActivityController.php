<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Activity;
use App\Http\Requests\V1\StoreActivityRequest;
use App\Http\Requests\V1\UpdateActivityRequest;
use App\Http\Requests\V1\DeleteActivityRequest;
use App\Http\Resources\V1\ActivityResource;
use App\Http\Resources\V1\ActivityCollection;
use Illuminate\Http\Request;
use App\Filters\V1\ActivitiesFilter;
use Illuminate\Support\Arr; //support for array maping
use App\Http\Requests\V1\BulkStoreActivityRequest;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = new ActivitiesFilter();
        $queryItems = $filter->transform($request); // ['column', 'operator', 'value']
        $perPage = $request->query('per_page', 10);

        if (count($queryItems) == 0) {
            return new ActivityCollection(Activity::paginate($perPage));
        }else {
            $activities = Activity::where($queryItems)->paginate($perPage);
            return new ActivityCollection($activities->appends($request->query()));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreActivityRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreActivityRequest $request)
    {
        return new ActivityResource(Activity::create($request->all()));
    }

    /**
     * Store a newly created bulk resource in storage.
     */
    public function bulkStore(BulkStoreActivityRequest $request)
    {
        $bulk = collect($request->all())->map(function($arr, $key){
            return Arr::except($arr, ['outputId','participantMale','participantFemale']);
        });
        Activity::insert($bulk->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        return new ActivityResource($activity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateActivityRequest  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateActivityRequest $request, Activity $activity)
    {
        if($activity->update($request->all())){
            return [
                'message' => 'Activity has been updated'
            ];
        }else{
            return [
                'message' => 'Error while updating Activity'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteActivityRequest $request, Activity $activity)
    {
        if($activity->delete($request->all())){
            return [
                'message' => 'Activity has been deleted'
            ];
        }else{
            return [
                'message' => 'Error while deleting Activity'
            ];
        }
    }
}
