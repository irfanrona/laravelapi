<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Subdepartment;
use App\Http\Requests\V1\StoreDepartmentRequest;
use App\Http\Requests\V1\UpdateDepartmentRequest;
use App\Http\Requests\V1\DeleteDepartmentRequest;
use App\Http\Resources\V1\DepartmentResource;
use App\Http\Resources\V1\DepartmentCollection;
use Illuminate\Http\Request;
use App\Filters\V1\DepartmentsFilter;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

//for generate Excel
// use Excel;
// use App\Exports\DepartmentExport;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = new DepartmentsFilter();
        $filterItems = $filter->transform($request); // ['column', 'operator', 'value']

        $includeSubdepartments = $request->query('includeSubdepartments');

        $departments = Department::where($filterItems);

        if ($includeSubdepartments) {
            $departments = $departments->with('subdepartments');
        }

        $perPage = $request->query('per_page', 10);

        return new DepartmentCollection($departments->paginate($perPage)->appends($request->query()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDepartmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepartmentRequest $request)
    {
        $originalFileName = $request->file('logoPicture') ? $request->file('logoPicture')->getClientOriginalName() : 'default.png';
        $extension = $request->file('logoPicture') ? $request->file('logoPicture')->getClientOriginalExtension() : 'png';

        $randomFileName = $request->file('logoPicture') ? uniqid() . '.' . $extension : 'default.png';

        $path = $request->file('logoPicture') ? Storage::putFileAs('public/images/logos', $request->file('logoPicture'), $randomFileName) : 'public/images/logos/default.png';
        
        // Modify the values in the $request object
        $request->merge([
            'logo_picture' => $path
        ]);

        $department = Department::create($request->all());
        if ($department->wasRecentlyCreated) {
            $subdepartmentData = [
                'name' => 'Main Department',
                'logo_picture' => $path
            ];
            
            $subdepartment = new Subdepartment($subdepartmentData);
            $department->subdepartments()->save($subdepartment);
            return new DepartmentResource($department);
        } else {
            return response()->json(['error' => 'Department record could not be created.'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        $includeSubdepartments = request()->query('includeSubdepartments');

        if ($includeSubdepartments) {
            return new DepartmentResource($department->loadMissing('subdepartments'));
        }
        return new DepartmentResource($department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDepartmentRequest  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDepartmentRequest $request, Department $department)
    {
        if ($request->file('logoPicture')) {
            $randomFileName = uniqid() . '.' . $request->file('logoPicture')->getClientOriginalExtension();

            $path = $request->file('logoPicture')->storeAs('public/images/logos', $randomFileName);

            if ($department->logoPicture != 'public/images/logos/default.jpg') {
                try {
                    Storage::disk('public')->delete($department->logoPicture); //not deleted
                } catch (\Throwable $e) {
                    return $e;
                }
                
                $request->merge([
                    'logo_picture' => $path,
                ]);
            }else{
                $request->merge([
                    'profile_picture' => 'public/images/logos/default.png',
                ]);
            } 
        }

        if($department->update($request->all())){
            return [
                'message' => 'Department has been updated'
            ];
        }else{
            return [
                'message' => 'Error while updating Department'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteDepartmentRequest $request, Department $department)
    {
        if($department->delete($request->all())){
            return [
                'message' => 'Department has been deleted'
            ];
        }else{
            return [
                'message' => 'Error while deleting Department'
            ];
        }
    }

    // public function generateExcelReport(Request $request)
    // {
    //     $filter = new DepartmentsFilter();
    //     $filterItems = $filter->transform($request); // ['column', 'operator', 'value']

    //     $includeSubdepartments = $request->query('includeSubdepartments');

    //     $departments = Department::where($filterItems);

    //     if ($includeSubdepartments) {
    //         $departments = $departments->with('subdepartments');
    //     }

    //     $data = new DepartmentCollection($departments->paginate()->appends($request->query()));

    //     // Use the Maatwebsite/Laravel-Excel package to export the data to Excel
    //     return Excel::download(new DepartmentExport($data), 'report.xlsx');
    // }
}
