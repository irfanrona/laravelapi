<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\V1\AuthLoginRequest;
use App\Models\User;
use App\Models\Role;
use Auth;

class AuthController extends Controller
{
    public function login(AuthLoginRequest $request)
    {
        if (!Auth::attempt($request->only('email', 'password')))
        {
            return response()
                ->json(['message' => 'Unauthorized'], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $role = Role::where('id', $user->role_id)->firstOrFail();

        // Delete existing personal access tokens
        $user->tokens->each(function ($token) {
            $token->delete();
        });

        // $token = $user->createToken('auth_token')->plainTextToken;
        $token = $user->createToken($role->name, json_decode($role->abilities,true))->plainTextToken;

        return response()
            ->json([
                'message' => 'Hi '.$user->name.', welcome to home',
                'id' => $user->id, 
                'name' => $user->name, 
                'access_token' => $token, 
                'token_type' => 'Bearer', 
            ]);
    }

    // method for user logout and delete token
    public function logout()
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'You have successfully logged out and the token was successfully deleted'
        ];
    }
}
