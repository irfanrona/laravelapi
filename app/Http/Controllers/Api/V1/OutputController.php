<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Output;
use App\Http\Requests\V1\StoreOutputRequest;
use App\Http\Requests\V1\UpdateOutputRequest;
use App\Http\Requests\V1\DeleteOutputRequest;
use App\Http\Resources\V1\OutputResource;
use App\Http\Resources\V1\OutputCollection;
use Illuminate\Http\Request;
use App\Filters\V1\OutputsFilter;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
//export
use App\Exports\OutputsExport;
use App\Exports\OutputsExportAll;
use Maatwebsite\Excel\Facades\Excel;

class OutputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = new OutputsFilter();
        $filterItems = $filter->transform($request); // ['column', 'operator', 'value']

        $includeActivities = $request->query('includeActivities');
        $sortBy = $request->query('sortBy');
        $done = $request->query('done');
        $onGoing = $request->query('onGoing');
        $createdBy = $request->query('createdBy');
        $departmentId = $request->query('departmentId');
        $subdepartmentId = $request->query('subdepartmentId');

        $outputs = Output::where($filterItems);

        if ($includeActivities) {
            $outputs = $outputs->with('activities');
        }

        if ($sortBy == 'latest') {
            $outputs = $outputs->latest('created_at');
        }

        if ($done) {
            $outputs = $outputs->where('percentage', 100);
        }

        if ($onGoing) {
            $outputs = $outputs->where('percentage', '<>', 100);
        }

        if ($createdBy) {
            $outputs = $outputs->where('created_by', $createdBy);
        }

        if ($departmentId) {
            $outputs = $outputs->where('department_id', $departmentId);
        }

        if ($subdepartmentId) {
            $outputs = $outputs->where('subdepartment_id', $subdepartmentId);
        }

        $perPage = $request->query('per_page', 10);

        return new OutputCollection($outputs->paginate($perPage)->appends($request->query()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOutputRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOutputRequest $request)
    {
        $originalFileName1 = $request->file('linkPhoto1') ? $request->file('linkPhoto1')->getClientOriginalName() : 'default.jpg';
        $originalFileName2 = $request->file('linkPhoto2') ? $request->file('linkPhoto2')->getClientOriginalName() : 'default.jpg';
        $originalFileName3 = $request->file('linkPhoto3') ? $request->file('linkPhoto3')->getClientOriginalName() : 'default.jpg';
        $originalFileName4 = $request->file('linkPhoto4') ? $request->file('linkPhoto4')->getClientOriginalName() : 'default.jpg';
        $extension1 = $request->file('linkPhoto1') ? $request->file('linkPhoto1')->getClientOriginalExtension() : 'jpg';
        $extension2 = $request->file('linkPhoto2') ? $request->file('linkPhoto2')->getClientOriginalExtension() : 'jpg';
        $extension3 = $request->file('linkPhoto3') ? $request->file('linkPhoto3')->getClientOriginalExtension() : 'jpg';
        $extension4 = $request->file('linkPhoto4') ? $request->file('linkPhoto4')->getClientOriginalExtension() : 'jpg';

        // Generate a random file name if profilePicture is not empty
        $randomFileName1 = $request->file('linkPhoto1') ? uniqid() . '.' . $extension1 : 'default.jpg';
        $randomFileName2 = $request->file('linkPhoto2') ? uniqid() . '.' . $extension2 : 'default.jpg';
        $randomFileName3 = $request->file('linkPhoto3') ? uniqid() . '.' . $extension3 : 'default.jpg';
        $randomFileName4 = $request->file('linkPhoto4') ? uniqid() . '.' . $extension4 : 'default.jpg';
        
        // Store the uploaded file with the random file name if profilePicture is not empty
        $path1 = $request->file('linkPhoto1') ? Storage::putFileAs('public/images/outputs', $request->file('linkPhoto1'), $randomFileName) : 'public/images/outputs/default.jpg';
        $path2 = $request->file('linkPhoto2') ? Storage::putFileAs('public/images/outputs', $request->file('linkPhoto2'), $randomFileName) : 'public/images/outputs/default.jpg';
        $path3 = $request->file('linkPhoto3') ? Storage::putFileAs('public/images/outputs', $request->file('linkPhoto3'), $randomFileName) : 'public/images/outputs/default.jpg';
        $path4 = $request->file('linkPhoto4') ? Storage::putFileAs('public/images/outputs', $request->file('linkPhoto4'), $randomFileName) : 'public/images/outputs/default.jpg';
        
        // Modify the values in the $request object
        $request->merge([
            'link_photo_1' => $path1,
            'link_photo_2' => $path2,
            'link_photo_3' => $path3,
            'link_photo_4' => $path4
        ]);

        $output = Output::create($request->except('sdgs'));
        $output->sdgs()->sync(collect($request->sdgs)->pluck('id')->all());
        return new OutputResource($output);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Output  $output
     * @return \Illuminate\Http\Response
     */
    public function show(Output $output)
    {
        $includeActivities = request()->query('includeActivities');

        if ($includeActivities) {
            return new OutputResource($output->loadMissing('activities'));
        }
        return new OutputResource($output);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOutputRequest  $request
     * @param  \App\Models\Output  $output
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOutputRequest $request, Output $output)
    {
        // return $request->all();
        if ($request->file('linkPhoto1')) {
            // Generate a random file name
            $randomFileName = uniqid() . '.' . $request->file('linkPhoto1')->getClientOriginalExtension();

            // Store the new profile picture file
            $path = $request->file('linkPhoto1')->storeAs('public/images/outputs', $randomFileName);

            // Delete the previous profile picture if it is not the default.jpg
            if ($output->linkPhoto1 != 'public/images/outputs/default.jpg') {
                try {
                    Storage::disk('public')->delete($output->linkPhoto1); //not deleted
                } catch (\Throwable $e) {
                    return $e;
                }
                
                
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'link_photo_1' => $path,
                ]);
            }else{
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'link_photo_1' => 'public/images/outputs/default.jpg',
                ]);
            } 
        }

        if ($request->file('linkPhoto2')) {
            // Generate a random file name
            $randomFileName = uniqid() . '.' . $request->file('linkPhoto2')->getClientOriginalExtension();

            // Store the new profile picture file
            $path = $request->file('linkPhoto2')->storeAs('public/images/outputs', $randomFileName);

            // Delete the previous profile picture if it is not the default.jpg
            if ($output->linkPhoto2 != 'public/images/outputs/default.jpg') {
                try {
                    Storage::disk('public')->delete($output->linkPhoto2); //not deleted
                } catch (\Throwable $e) {
                    return $e;
                }
                
                
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'link_photo_2' => $path,
                ]);
            }else{
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'link_photo_2' => 'public/images/outputs/default.jpg',
                ]);
            } 
        }

        if ($request->file('linkPhoto3')) {
            // Generate a random file name
            $randomFileName = uniqid() . '.' . $request->file('linkPhoto3')->getClientOriginalExtension();

            // Store the new profile picture file
            $path = $request->file('linkPhoto3')->storeAs('public/images/outputs', $randomFileName);

            // Delete the previous profile picture if it is not the default.jpg
            if ($output->linkPhoto3 != 'public/images/outputs/default.jpg') {
                try {
                    Storage::disk('public')->delete($output->linkPhoto3); //not deleted
                } catch (\Throwable $e) {
                    return $e;
                }
                
                
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'link_photo_3' => $path,
                ]);
            }else{
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'link_photo_3' => 'public/images/outputs/default.jpg',
                ]);
            } 
        }

        if ($request->file('linkPhoto4')) {
            // Generate a random file name
            $randomFileName = uniqid() . '.' . $request->file('linkPhoto4')->getClientOriginalExtension();

            // Store the new profile picture file
            $path = $request->file('linkPhoto4')->storeAs('public/images/outputs', $randomFileName);

            // Delete the previous profile picture if it is not the default.jpg
            if ($output->linkPhoto4 != 'public/images/outputs/default.jpg') {
                try {
                    Storage::disk('public')->delete($output->linkPhoto4); //not deleted
                } catch (\Throwable $e) {
                    return $e;
                }
                
                
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'link_photo_4' => $path,
                ]);
            }else{
                // Update the profilePicture field in the $request object with the new file path
                $request->merge([
                    'link_photo_4' => 'public/images/outputs/default.jpg',
                ]);
            } 
        }

        if($output->update($request->except('sdgs'))){
            if ($request->has('sdgs')) {
                if ($output->sdgs()->sync(collect($request->sdgs)->pluck('id')->all())) {
                    return [
                        'message' => 'Output has been updated'
                    ];
                } else {
                    return [
                        'message' => 'Error while updating Output sdgs'
                    ];
                }
            } else {
                return [
                    'message' => 'Output has been updated'
                ];
            }
        } else {
            return [
                'message' => 'Error while updating Output'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Output  $output
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteOutputRequest $request, Output $output)
    {
        if($output->delete($request->all())){
            return [
                'message' => 'Output has been deleted'
            ];
        }else{
            return [
                'message' => 'Error while deleting Output'
            ];
        }
    }

    public function export($id = 'all') 
    {
        if ($id == 'all') {
            return Excel::download(new OutputsExportAll, 'outputs.pdf', \Maatwebsite\Excel\Excel::MPDF);
        }else{
            $output = Output::findOrFail($id);
            $outputResource = new OutputResource($output->loadMissing('activities'));

            return Excel::download(new OutputsExport($outputResource), 'outputs.pdf', \Maatwebsite\Excel\Excel::MPDF);
        }
        
        // return Excel::download(new OutputsExport, 'outputs.xlsx');
    }
}
