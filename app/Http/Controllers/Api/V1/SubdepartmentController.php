<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Subdepartment;
use App\Http\Requests\V1\StoreSubdepartmentRequest;
use App\Http\Requests\V1\UpdateSubdepartmentRequest;
use App\Http\Requests\V1\DeleteSubdepartmentRequest;
use App\Http\Resources\V1\SubdepartmentResource;
use App\Http\Resources\V1\SubdepartmentCollection;
use Illuminate\Http\Request;
use App\Filters\V1\SubdepartmentsFilter;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class SubdepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = new SubdepartmentsFilter();
        $queryItems = $filter->transform($request); // ['column', 'operator', 'value']
        $perPage = $request->query('per_page', 10);

        if ($request->has('departmentId')) {
            $queryItems[] = ['department_id', '=', $request->input('departmentId')];
        }
        
        if (count($queryItems) == 0) {
            return new SubdepartmentCollection(Subdepartment::paginate($perPage));
        }else {
            $subdepartments = Subdepartment::where($queryItems)->paginate($perPage);
            return new SubdepartmentCollection($subdepartments->appends($request->query()));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSubdepartmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubdepartmentRequest $request)
    {
        $originalFileName = $request->file('logoPicture') ? $request->file('logoPicture')->getClientOriginalName() : 'default.png';
        $extension = $request->file('logoPicture') ? $request->file('logoPicture')->getClientOriginalExtension() : 'png';

        $randomFileName = $request->file('logoPicture') ? uniqid() . '.' . $extension : 'default.png';

        $path = $request->file('logoPicture') ? Storage::putFileAs('public/images/logos', $request->file('logoPicture'), $randomFileName) : 'public/images/logos/default.png';
        
        // Modify the values in the $request object
        $request->merge([
            'logo_picture' => $path
        ]);
        
        return new SubdepartmentResource(Subdepartment::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subdepartment  $subdepartment
     * @return \Illuminate\Http\Response
     */
    public function show(Subdepartment $subdepartment)
    {
        return new SubdepartmentResource($subdepartment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSubdepartmentRequest  $request
     * @param  \App\Models\Subdepartment  $subdepartment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubdepartmentRequest $request, Subdepartment $subdepartment)
    {
        if ($request->file('logoPicture')) {
            $randomFileName = uniqid() . '.' . $request->file('logoPicture')->getClientOriginalExtension();

            $path = $request->file('logoPicture')->storeAs('public/images/logos', $randomFileName);

            if ($subdepartment->logoPicture != 'public/images/logos/default.jpg') {
                try {
                    Storage::disk('public')->delete($subdepartment->logoPicture); //not deleted
                } catch (\Throwable $e) {
                    return $e;
                }
                
                $request->merge([
                    'logo_picture' => $path,
                ]);
            }else{
                $request->merge([
                    'profile_picture' => 'public/images/logos/default.png',
                ]);
            } 
        }

        if($subdepartment->update($request->all())){
            return [
                'message' => 'Subdepartment has been updated'
            ];
        }else{
            return [
                'message' => 'Error while updating Subdepartment'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subdepartment  $subdepartment
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteSubdepartmentRequest $request, Subdepartment $subdepartment)
    {
        if($subdepartment->delete($request->all())){
            return [
                'message' => 'Subdepartment has been deleted'
            ];
        }else{
            return [
                'message' => 'Error while deleting Subdepartment'
            ];
        }
    }
}
