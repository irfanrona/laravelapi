<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Sdg;
use App\Http\Requests\V1\StoreSdgRequest;
use App\Http\Requests\V1\UpdateSdgRequest;
use App\Http\Requests\V1\DeleteSdgRequest;
use App\Http\Resources\V1\SdgResource;
use App\Http\Resources\V1\SdgCollection;
use Illuminate\Http\Request;
use App\Filters\V1\SdgsFilter;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class SdgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = new SdgsFilter();
        $queryItems = $filter->transform($request); // ['column', 'operator', 'value']

        if (count($queryItems) == 0) {
            return new SdgCollection(Sdg::paginate());
        }else {
            $sdgs = Sdg::where($queryItems)->paginate();
            return new SdgCollection($sdgs->appends($request->query()));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSdgRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSdgRequest $request)
    {
        return new SdgResource(Sdg::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sdg  $sdg
     * @return \Illuminate\Http\Response
     */
    public function show(Sdg $sdg)
    {
        return new SdgResource($sdg);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSdgRequest  $request
     * @param  \App\Models\Sdg  $sdg
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSdgRequest $request, Sdg $sdg)
    {
        if($sdg->update($request->all())){
            return [
                'message' => 'Sdg has been updated'
            ];
        }else{
            return [
                'message' => 'Error while updating Sdg'
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sdg  $sdg
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteSdgRequest $request, Sdg $sdg)
    {
        if($sdg->delete($request->all())){
            return [
                'message' => 'Sdg has been deleted'
            ];
        }else{
            return [
                'message' => 'Error while deleting Sdg'
            ];
        }
    }
}
