<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'logo_picture'
    ];

    public function subdepartments()
    {
        return $this->hasMany(Subdepartment::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
