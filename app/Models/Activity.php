<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $fillable = [
        'output_id',
        'name',
        'from',
        'to',
        'status',
        'participant_male',
        'participant_female',
        'time',
        'result'
    ];

    public function outputs()
    {
        return $this->belongsTo(Output::class);
    }
}
