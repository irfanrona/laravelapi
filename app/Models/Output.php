<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Output extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'department_id',
        'subdepartment_id',
        'percentage',
        'location',
        'challange',
        'follow_up',
        'link_report',
        'link_photo_1',
        'link_photo_2',
        'link_photo_3',
        'link_photo_4',
        'desc_photo_1',
        'desc_photo_2',
        'desc_photo_3',
        'desc_photo_4',
        'created_by'
    ];

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public function sdgs()
    {
        return $this->belongsToMany(Sdg::class);
    }

}
