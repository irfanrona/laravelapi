<?php

namespace App\Filters\V1;

use Illuminate\Http\Request;
use App\Filters\ApiFilter;

class SubdepartmentsFilter extends ApiFilter
{
    protected $allowedParms = [
        'department_id' => ['eq'],
        'name' => ['eq']
    ];

    protected $operatorMap = [
        'eq' => '='
    ];

}
