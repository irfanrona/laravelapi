<?php

namespace App\Filters\V1;

use Illuminate\Http\Request;
use App\Filters\ApiFilter;

class OutputsFilter extends ApiFilter
{
    protected $allowedParms = [
        'name' => ['eq'],
        'location' => ['eq'],
        'percentage' => ['eq','lt','gt','lte','gte']
    ];

    protected $operatorMap = [
        'eq' => '=',
        'lt' => '<',
        'lte' => '<=',
        'gt' => '>',
        'gte' => '>=',
        'ne' => '!='
    ];
}
