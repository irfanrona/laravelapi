<?php

namespace App\Filters\V1;

use Illuminate\Http\Request;
use App\Filters\ApiFilter;

class ActivitiesFilter extends ApiFilter
{
    protected $allowedParms = [
        'output_id' => ['eq'],
        'name' => ['eq']
    ];

    protected $operatorMap = [
        'eq' => '='
    ];

}
