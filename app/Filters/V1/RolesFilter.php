<?php

namespace App\Filters\V1;

use Illuminate\Http\Request;
use App\Filters\ApiFilter;

class RolesFilter extends ApiFilter
{
    protected $allowedParms = [
        'name' => ['eq']
    ];

    protected $operatorMap = [
        'eq' => '='
    ];
}
