<?php

namespace App\Filters\V1;

use Illuminate\Http\Request;
use App\Filters\ApiFilter;

class UsersFilter extends ApiFilter
{
    protected $allowedParms = [
        'department_id' => ['eq'],
        'subdepartment_id' => ['eq'],
        'role_id' => ['eq'],
        'name' => ['eq'],
        'email' => ['eq'],
        'job_title' => ['eq']
    ];

    protected $operatorMap = [
        'eq' => '='
    ];

}
