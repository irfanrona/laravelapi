<?php

namespace App\Exports;

use App\Models\Output;
use App\Models\Department;
use App\Models\Subdepartment;
use App\Http\Resources\V1\OutputResource;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class OutputsExportAll implements FromCollection, WithCustomStartCell, WithHeadings, WithStyles, WithDrawings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {      
        $outputs = Output::all();
        $outputs = OutputResource::collection($outputs);
        $i = 1;

        $rows = $outputs->map(function ($output) use (&$i) {
            $percentageText = '';
            $departmentName = Department::find($output->department_id)->name ?? '';
            $subdepartmentName = Subdepartment::find($output->subdepartment_id)->name ?? '';

            if ($output->percentage == 100) {
                $percentageText = 'done';
                $fontColor = '00FF00'; // Green
            } elseif ($output->percentage > 0) {
                $percentageText = 'progress';
                $fontColor = 'FFA500'; // Orange
            } else {
                $percentageText = 'not done';
                $fontColor = 'FF0000'; // Red
            }

            $name = $departmentName . " - " . $subdepartmentName;

            return [
                $i++,
                $name,
                $percentageText,
                $output->name,
                $output->location,
                $output->link_report
            ];
        });

        return $rows;
    }

    public function headings(): array
    {
        return [
            'No',
            'Department',
            'Status',
            'Name',
            'Location',
            'Link Report'
        ];
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('storage/images/logos/logopt.png'));
        $drawing->setHeight(200);
        $drawing->setCoordinates('B2');

        return $drawing;
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->mergeCells('B2:C3');

        // $sheet->getStyle('D2:G3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D2:G3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('D2:G2');
        $sheet->mergeCells('D3:G3');
        $sheet->setCellValue('D2', 'PT. Rimba Raya Conservation');
        $sheet->setCellValue('D3', 'All Output Reports');
        $sheet->getStyle('D2')->getFont()->setSize(50);
        $sheet->getStyle('D3')->getFont()->setSize(30);

        // Set the paper size to A4 for PDF export
        $sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $outputs = Output::all();
        $outputs = OutputResource::collection($outputs);

        $sheet->getStyle('B9:G9')->getFont()->setBold(true);
        // Set gray background color for the header row
        $headerRange = 'B9:G9'; // Assuming the header row is row 9
        $sheet->getStyle($headerRange)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');

        // $sheet->getStyle('C')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFF0F0F0');
        // Calculate the last row dynamically based on the number of rows in the collection
        $lastRow = 9 + count($outputs);

        // Set the range of cells from B9 to G(lastRow) for border style
        $range = 'B9:G' . $lastRow;
        $range_body = 'B10:G' . $lastRow;

        // Apply the border style to the range of cells
        $sheet->getStyle($range)->getAlignment()->setWrapText(true);
        $sheet->getStyle($range)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle($range)->getAlignment()->setIndent(2);

        // Set the column width for columns B to G
        $sheet->getColumnDimension('B')->setWidth(10); // Set the desired width (in characters)
        $sheet->getColumnDimension('C')->setWidth(60);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(50);
        $sheet->getColumnDimension('F')->setWidth(50);
        $sheet->getColumnDimension('G')->setWidth(55);

        $sheet->getRowDimension(9)->setRowHeight(30); // Set the desired height (in points)
        $startingRow = 9; // Starting row of the range
        $endingRow = $lastRow; // Ending row of the range
        $rowHeight = 40; // Desired row height in points

        for ($row = $startingRow; $row <= $endingRow; $row++) {
            $sheet->getRowDimension($row)->setRowHeight($rowHeight);
        }

        // Set alignment and font style for the cells
        $sheet->getStyle($range)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->getStyle($range)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle($range)->getFont()->setSize(12); // Set the desired font size (in points)

         // Set font color based on value in each row
        $valueColumnStatus = 'D';
        $valueColumnLink = 'G';
        $progressColor = 'DDA522';
        $doneColor = '22AA22';
        $notDoneColor = 'FF0000';
        $linkColor = '0000FF';
        
        for ($row = 10; $row <= $lastRow; $row++) {
            $cell = $valueColumnStatus . $row;
            $cellValue = $sheet->getCell($cell)->getValue();
            
            if ($cellValue === 'progress') {
                $sheet->getStyle($cell)->getFont()->getColor()->setARGB($progressColor);
            }else if ($cellValue === 'not done') {
                $sheet->getStyle($cell)->getFont()->getColor()->setARGB($notDoneColor);
            }else if ($cellValue === 'done') {
                $sheet->getStyle($cell)->getFont()->getColor()->setARGB($doneColor);
            }
            $sheet->getStyle($valueColumnLink . $row)->getFont()->getColor()->setARGB($linkColor);
        }
    }

    public function startCell(): string
    {
        return 'B9';
    }

}
