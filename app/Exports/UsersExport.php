<?php

namespace App\Exports;

use App\Models\User;
use App\Http\Resources\V1\UserResource;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class UsersExport implements FromCollection, WithCustomStartCell, WithHeadings, WithStyles, WithDrawings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $users = User::all();
        return UserResource::collection($users);
    }

    public function headings(): array
    {
        return [
            'Id User',
            'Id Department',
            'Id Subdepartment',
            'Id Role',
            'Name',
            'Job Title',
            'Email'
        ];
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('storage/images/logos/default.png'));
        $drawing->setHeight(90);
        $drawing->setCoordinates('B2');

        return $drawing;
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('B8')->getFont()->setBold(true);
    }

    public function startCell(): string
    {
        return 'B9';
    }

}
