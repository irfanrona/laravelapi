<?php

namespace App\Exports;

use App\Models\Output;
use App\Models\Department;
use App\Models\Subdepartment;
use App\Http\Resources\V1\OutputResource;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class OutputsExport implements WithCustomStartCell, WithStyles, WithDrawings
{
    protected $outputResource;
    protected $departmentName;
    protected $subdepartmentName;

    public function __construct(OutputResource $outputResource)
    {
        $this->outputResource = $outputResource;
        $this->departmentName = Department::find($outputResource->department_id)->name ?? '';
        $this->subdepartmentName = Subdepartment::find($outputResource->subdepartment_id)->name ?? '';
    }

    public function collection()
    {      
        $this->outputResource = $this->outputResource->toArray();
    }

    public function drawings()
    {
        $drawings = [];

        $drawingLogo = new Drawing();
        $drawingLogo->setName('Logo');
        $drawingLogo->setDescription('This is my logo');
        $drawingLogo->setPath(public_path('storage/images/logos/logopt.png'));
        $drawingLogo->setHeight(200);
        $drawingLogo->setCoordinates('B2');
        $drawings[] = $drawingLogo;

        $lastRow = 19 + count($this->outputResource->activities);
        $lastRow1 = $lastRow+2;

        $drawing1 = new Drawing();
        $drawing1->setName('OutputImage1');
        $drawing1->setDescription('OutputImage1');
        $drawing1->setPath(public_path(Storage::url($this->outputResource->link_photo_1)));
        $drawing1->setHeight(200);
        $drawing1->setCoordinates('B'.$lastRow);
        $drawings[] = $drawing1;

        $drawing1 = new Drawing();
        $drawing1->setName('OutputImage1');
        $drawing1->setDescription('OutputImage1');
        $drawing1->setPath(public_path(Storage::url($this->outputResource->link_photo_2)));
        $drawing1->setHeight(200);
        $drawing1->setCoordinates('F'.$lastRow);
        $drawings[] = $drawing1;

        $drawing1 = new Drawing();
        $drawing1->setName('OutputImage1');
        $drawing1->setDescription('OutputImage1');
        $drawing1->setPath(public_path(Storage::url($this->outputResource->link_photo_3)));
        $drawing1->setHeight(200);
        $drawing1->setCoordinates('B'.$lastRow1);
        $drawings[] = $drawing1;

        $drawing1 = new Drawing();
        $drawing1->setName('OutputImage1');
        $drawing1->setDescription('OutputImage1');
        $drawing1->setPath(public_path(Storage::url($this->outputResource->link_photo_4)));
        $drawing1->setHeight(200);
        $drawing1->setCoordinates('F'.$lastRow1);
        $drawings[] = $drawing1;

        return $drawings;
    }

    public function styles(Worksheet $sheet)
    {
        
        $sheet->mergeCells('B2:C3');

        // $sheet->getStyle('D2:G3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D2:G3')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('D2:G2');
        $sheet->mergeCells('D3:G3');
        $sheet->setCellValue('D2', 'PT. Rimba Raya Conservation');
        $sheet->setCellValue('D3', $this->departmentName. ' - ' .$this->subdepartmentName);
        $sheet->getStyle('D2')->getFont()->setSize(50);
        $sheet->getStyle('D3')->getFont()->setSize(30);

        // Set the paper size to A4 for PDF export
        $sheet->getPageSetup()->setPaperSize(PageSetup::PAPERSIZE_A4);

        $sheet->getStyle('B9:G9')->getFont()->setBold(true);

        // Set the range for the data row
        $startRow = 10;
        $endRow = 10;

        // Set the column width for columns B to G
        $sheet->getColumnDimension('B')->setWidth(50);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(40);
        $sheet->getColumnDimension('F')->setWidth(20);
        $sheet->getColumnDimension('G')->setWidth(100);

        // Set font color based on value in the row
        $valueColumnStatus = 'D';
        $valueColumnLink = 'G';
        $progressColor = 'DDA522';
        $doneColor = '22AA22';
        $notDoneColor = 'FF0000';
        $linkColor = '0000FF';

        $cell = 'G10:G11';
        $cellValue = $this->outputResource->percentage;

        if ($cellValue == 100) {
            $sheet->getStyle($cell)->getFont()->getColor()->setARGB($doneColor);
            $status = 'Done';
        } elseif ($cellValue > 0) {
            $sheet->getStyle($cell)->getFont()->getColor()->setARGB($progressColor);
            $status = 'Progress';
        } else {
            $sheet->getStyle($cell)->getFont()->getColor()->setARGB($notDoneColor);
            $status = 'Not Done';
        }

        $range = 'B9:G15';
        $sheet->getStyle($range)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->getStyle($range)->getFont()->setSize(14);
        

        $sheet->getStyle('B9:E9')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');
        $sheet->mergeCells('C9:E9');
        $sheet->mergeCells('C10:E10');
        $sheet->getStyle('B11:E11')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');
        $sheet->mergeCells('C11:E11');
        $sheet->mergeCells('C12:E12');
        $sheet->getStyle('B13:E13')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');
        $sheet->mergeCells('C13:E13');

        $sheet->getStyle('C11')->getFont()->getColor()->setARGB($linkColor);

        $sheet->setCellValue('B9', '   Output Program');
        $sheet->setCellValue('C9', $this->outputResource->name);
        $sheet->setCellValue('B10', '   Location');
        $sheet->setCellValue('C10', $this->outputResource->location);
        $sheet->setCellValue('B11', '   Link Report');
        $sheet->setCellValue('C11', $this->outputResource->link_report);
        $sheet->setCellValue('B12', '   Follow Up');
        $sheet->setCellValue('C12', $this->outputResource->follow_up);
        $sheet->setCellValue('B13', '   Challange');
        $sheet->setCellValue('C13', $this->outputResource->challange);

        $sheet->mergeCells('G10:G11');
        $sheet->getStyle('G9:G10')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
        $sheet->getStyle('G9')->getFont()->setSize(18);
        $sheet->getStyle('G10:G11')->getFont()->setSize(38);
        $sheet->setCellValue('G9', '   Status');
        $sheet->setCellValue('G10', $status);

        $sheet->setCellValue('B15', '   Activities');
        $sheet->getStyle('B15')->getFont()->setSize(18);
        $lastRow = 16 + count($this->outputResource->activities);

        // Set the range of cells from B9 to G(lastRow) for border style
        $range = 'B16:G' . $lastRow;
        $sheet->getStyle('B16:G16')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');
        $sheet->getStyle($range)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getRowDimension(16)->setRowHeight(30);
        
        // Set alignment and font style for the cells
        $sheet->getStyle($range)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->getStyle($range)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle($range)->getFont()->setSize(12); // Set the desired font size (in points)
        $sheet->setCellValue('B16', 'ACTIVITY NAME');
        $sheet->setCellValue('C16', 'PERIODE (FORM - TO)');
        $sheet->setCellValue('D16', 'STATUS');
        $sheet->setCellValue('E16', 'PARTICIPANT');
        $sheet->setCellValue('F16', 'TIME');
        $sheet->setCellValue('G16', 'RESULT');
        $row = 17; // Starting row for activities

        foreach ($this->outputResource->activities as $activity) {
            $cell = 'B' . $row;
            $participant = $activity->participant_male . ' Male, '. $activity->participant_female . ' Female';
            $sheet->setCellValue($cell, $activity->name);
            $sheet->setCellValue('C'.$row, $activity->from . '-' .$activity->to);
            $sheet->setCellValue('D'.$row, $activity->status);
            $sheet->setCellValue('E'.$row, $participant);
            $sheet->setCellValue('F'.$row, $activity->time);
            $sheet->setCellValue('G'.$row, $activity->result);
            $row++;
        }

        for ($start = 9; $start <= $row; $start++) {
            $sheet->getRowDimension($start)->setRowHeight(40);
        }

        $rowAfterActivities = $lastRow+3;

        //Photos
        $range = 'B' . ($rowAfterActivities-1) . ':G' . ($rowAfterActivities+4);
        $sheet->getStyle($range)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFEEEEEE');

        for ($i=0; $i < 4; $i++) { 
            $rangeLeft = 'B' . ($rowAfterActivities+$i) . ':E' . ($rowAfterActivities+$i);
            $sheet->mergeCells($rangeLeft);
            $rangeRight = 'F' . ($rowAfterActivities+$i) . ':G' . ($rowAfterActivities+$i);
            $sheet->mergeCells($rangeRight);
        }

        $rowDesc = $rowAfterActivities+1;
        $descEmpty = 'No description available';
        $cellValue = !empty($this->outputResource->desc_photo_1) ? $this->outputResource->desc_photo_1 : $descEmpty;
        $sheet->setCellValue('B'. $rowDesc, $cellValue);
        $cellValue = !empty($this->outputResource->desc_photo_2) ? $this->outputResource->desc_photo_2 : $descEmpty;
        $sheet->setCellValue('F'. $rowDesc, $cellValue);
        $rowDesc = $rowDesc+2;
        $cellValue = !empty($this->outputResource->desc_photo_3) ? $this->outputResource->desc_photo_3 : $descEmpty;
        $sheet->setCellValue('B'. $rowDesc, $cellValue);
        $cellValue = !empty($this->outputResource->desc_photo_4) ? $this->outputResource->desc_photo_4 : $descEmpty;
        $sheet->setCellValue('F'. $rowDesc, $cellValue);
        
        for ($row = $rowAfterActivities; $row <= $rowDesc; $row++) {
            if ($row == $rowAfterActivities) {
                $sheet->getRowDimension($row)->setRowHeight(200);
            }else{
                $sheet->getRowDimension($row)->setRowHeight(100);
            }
        }
        
        $sheet->getStyle($range)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->getStyle($range)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        //End of Photos

        //SDGS
        $lastRow = $rowDesc+4;
        $sheet->setCellValue('B'. $lastRow, '   SDGS');
        $sheet->getStyle('B'. $lastRow)->getFont()->setSize(22);
        $lastRow+=2;

        foreach ($this->outputResource->sdgs as $sdg) {
            $cell = 'B' . $lastRow;
            $sheet->mergeCells($cell.':G'.$lastRow);
            $sheet->setCellValue($cell, '    '.$sdg->id.  ' '. $sdg->name);
            $sheet->getStyle($cell)->getFont()->setSize(18);
            $sheet->getRowDimension($lastRow)->setRowHeight(30);
            $sheet->getStyle($cell)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

            if ($lastRow % 2 == 1) {
                $sheet->getStyle($cell)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFEEEEEE');
            }
            $lastRow++;
        }

    }

    public function startCell(): string
    {
        return 'B9';
    }

}
