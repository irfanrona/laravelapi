<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=> 'v1', 'namespace' => 'App\Http\Controllers\Api\V1'], function () {
    Route::post('auth/login', ['uses' => 'AuthController@login']);
    Route::get('outputs/export/{id}', ['uses' => 'OutputController@export']);
});

Route::group(['prefix'=> 'v1', 'namespace' => 'App\Http\Controllers\Api\V1', 'middleware' => 'auth:sanctum'], function()
{
    Route::get('auth/logout', ['uses' => 'AuthController@logout']);
    Route::get('departments/a', ['uses' => 'DepartmentController@generateExcelReport']);
    // Add the new route for /users/me
    Route::get('users/me', ['uses' => 'UserController@me']);
    Route::get('users/export/', ['uses' => 'UserController@export']);
    // route for output PDF
    // Route::get('outputs/export/{id}', ['uses' => 'OutputController@export']);
    // Route::apiResource('customers', CustomerController::class);
    // Route::apiResource('invoices', InvoiceController::class);
    Route::apiResource('users', UserController::class);
    Route::apiResource('roles', RoleController::class);
    Route::apiResource('departments', DepartmentController::class);
    Route::apiResource('subdepartments', SubdepartmentController::class);
    Route::apiResource('outputs', OutputController::class);
    Route::apiResource('activities', ActivityController::class);
    Route::post('activities/bulk', ['uses' => 'ActivityController@bulkStore']);

    

    // Route::post('invoices/bulk', ['uses' => 'InvoiceController@bulkStore']);
});