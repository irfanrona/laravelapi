<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::factory()
        ->count(1)
        ->hasSubdepartments(2)
        ->create();

        Department::factory()
        ->count(1)
        ->hasSubdepartments(1)
        ->create();
    }
}
