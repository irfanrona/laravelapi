<?php

namespace Database\Seeders;

use App\Models\Output;
use App\Models\Sdg;
use Illuminate\Database\Seeder;

class OutputSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create 20 Outputs, each with 5 activities and 1-3 SDGs
        Output::factory()
            ->count(20)
            ->hasActivities(5)
            ->create()
            ->each(function ($output) {
                $output->sdgs()->attach(Sdg::inRandomOrder()->take(rand(1, 3))->pluck('id')->toArray());
            });

        // create 10 Outputs, each with 2 activities and 1-3 SDGs
        Output::factory()
            ->count(10)
            ->hasActivities(2)
            ->create()
            ->each(function ($output) {
                $output->sdgs()->attach(Sdg::inRandomOrder()->take(rand(5, 8))->pluck('id')->toArray());
            });

        // create 5 Outputs, each with no activities and 1-3 SDGs
        Output::factory()
            ->count(5)
            ->create()
            ->each(function ($output) {
                $output->sdgs()->attach(Sdg::inRandomOrder()->take(rand(3, 7))->pluck('id')->toArray());
            });
    }
}
