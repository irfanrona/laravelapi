<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'department_id' => 1,
            'subdepartment_id' => 1,
            'role_id' => 1,
            'name' => 'Owner',
            'email' => 'admin@rimbaraya.com',
            'email_verified_at' => now(),
            'job_title' => 'Owner',
            'profile_picture' => 'public/images/profiles/default.jpg',
            'password' => '$2y$10$1EX7l3LahH5TtDOA3CsdWe/qoe/66u5Rwz4s.7NTyRces8DnfbD4W', // admin
        ]);
        
        User::factory()
        ->count(2)
        ->create();
    }
}
