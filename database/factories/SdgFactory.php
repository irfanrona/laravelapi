<?php

namespace Database\Factories;

use App\Models\Sdg;
use Illuminate\Database\Eloquent\Factories\Factory;

class SdgFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sdg_list = [
            'No Poverty',
            'Zero Hunger',
            'Good Health and Well Being',
            'Quality Education',
            'Gender Equality',
            'Clean Water and Sanitation',
            'Affordable and Clean energy',
            'Decent Work and Economic Growth',
            'Industry, Innovation and Infratructure',
            'Reduced Inequalities',
            'Sustainablecities and Communities',
            'Responsible Consumption and Production',
            'Climate Action',
            'Life Below Water',
            'Life on Land',
            'Peace, Justice and Strong Institutions',
            'Partnerships for The Goals'
        ];

        static $counter = 0;
        $name = $sdg_list[$counter];
        $counter++;

        return [
            'name' => $name,
        ];
    }
}
