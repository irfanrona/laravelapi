<?php

namespace Database\Factories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        //This is the ALL Abilities
        // abilities' => '["user:create","department:create","subdepartment:create","output:create","activity:create","user:update","department:update","subdepartment:update","output:update","activity:update","user:delete","department:delete","subdepartment:delete","output:delete","activity:delete","report:all","report:owned","role:all"]'
        $roleList = [
            [
                'name' => 'Head Department',
                'abilities' => '["user:create","department:create","subdepartment:create","output:create","activity:create","user:update","department:update","subdepartment:update","output:update","activity:update","user:delete","department:delete","subdepartment:delete","output:delete","activity:delete","report:owned"]'
            ],
            [
                'name' => 'Staff',
                'abilities' => '["user:create","output:create","activity:create","user:update","department:update","subdepartment:update","output:update","activity:update","output:delete","activity:delete"]'
            ],
            [
                'name' => 'Employee',
                'abilities' => '["output:create","activity:create","user:update","output:update","activity:update"]'
            ],
        ];

        return $this->faker->unique()->randomElement($roleList);
    }
}
