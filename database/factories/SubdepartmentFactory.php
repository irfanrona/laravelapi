<?php

namespace Database\Factories;

use App\Models\Department;
use App\Models\Subdepartment;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubdepartmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $subdepartment_list = [
            'Fire and Forest Protection Program',
            'Nursery & Planting Program',
            'Biodiversity Program',
            'Health Program',
            'Alternative Livelihood Program',
            'Education and Literacy Program',
            'Junior GIS Specialist',
            'HR and GA Staff'
        ];
        
        return [
            'department_id' => Department::factory(),
            'name' => $this->faker->unique()->randomElement($subdepartment_list),
            'logo_picture' => 'public/images/logos/default.png',
        ];
    }
}
