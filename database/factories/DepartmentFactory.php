<?php

namespace Database\Factories;

use App\Models\Department;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepartmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $department_list = [
            'Forest and Landscape Relation',
            'Community Development',
            'Senior Gist Specialist',
            'Human Resource',
            'General',
            'Production',
            'Technology'
        ];

        return [
            'name' => $this->faker->unique()->randomElement($department_list),
            'logo_picture' => 'public/images/logos/default.png',
        ];
    }
}
