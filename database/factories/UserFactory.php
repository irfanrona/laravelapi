<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Department;
use App\Models\Subdepartment;
use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $role = Role::inRandomOrder()->first(); // Fetch a random role from the database

        return [
            'department_id' => Department::factory(),
            'subdepartment_id' => Subdepartment::factory(),
            'role_id' => $role,
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'job_title' => $this->faker->jobTitle(),
            'profile_picture' => 'public/images/profiles/default.jpg',
            'password' => '$2y$10$1EX7l3LahH5TtDOA3CsdWe/qoe/66u5Rwz4s.7NTyRces8DnfbD4W', // admin
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
