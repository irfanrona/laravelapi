<?php

namespace Database\Factories;

use App\Models\Activity;
use App\Models\Output;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->streetName() . " " . $this->faker->company();
        $quartal = $this->faker->randomElement(['Q1','Q2','Q3','Q4']);
        $status = $this->faker->randomElement(['progress','delay','finish']);

        return [
            'output_id' => Output::factory(),
            'name' => $this->faker->words(3,true),
            'from' => $quartal,
            'to' => $quartal,
            'status' => $status,
            'participant_male' => $this->faker->numberBetween(0,10),
            'participant_female' => $this->faker->numberBetween(0,5),
            'time' => $this->faker->words(2,true),
            'result' => $this->faker->paragraph(1,true)
        ];
    }
}
