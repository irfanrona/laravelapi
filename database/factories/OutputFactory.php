<?php

namespace Database\Factories;

use App\Models\Output;
use App\Models\User;
use App\Models\Sdg;
use Illuminate\Database\Eloquent\Factories\Factory;

class OutputFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->streetName() . " " . $this->faker->company();
        $user = User::inRandomOrder()->select('*')->first();
        $randomNumber = $this->faker->numberBetween(0, 100);

        // Adjust the probability distribution
        if ($randomNumber < 20) {
            $randomNumber = 0; // Increase the chance of getting 0
        } elseif ($randomNumber > 80) {
            $randomNumber = 100; // Increase the chance of getting 100
        }

        return [
            'department_id' => $user->department_id,
            'subdepartment_id' => $user->subdepartment_id,
            'name' => $name,
            'percentage' => $randomNumber,
            'location' => $this->faker->streetAddress(),
            'challange' => $this->faker->paragraph(1,true),
            'follow_up' => $this->faker->paragraph(1,true),
            'link_report' => $this->faker->url(),
            'link_photo_1' => 'public/images/outputs/default.jpg',
            'link_photo_2' => 'public/images/outputs/default.jpg',
            'link_photo_3' => 'public/images/outputs/default.jpg',
            'link_photo_4' => 'public/images/outputs/default.jpg',
            'desc_photo_1' => $this->faker->paragraph(1,true),
            'desc_photo_2' => $this->faker->paragraph(1,true),
            'desc_photo_3' => $this->faker->paragraph(1,true),
            'desc_photo_4' => $this->faker->paragraph(1,true),
            'created_by' => $user->id
        ];
    }
}
