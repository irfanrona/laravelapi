<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('output_id');
            $table->string('name');
            $table->string('from');
            $table->string('to');
            $table->string('status');
            $table->integer('participant_male');
            $table->integer('participant_female');
            $table->string('time')->nullable();
            $table->string('result')->nullable();
            $table->timestamps();

            $table->foreign('output_id')->references('id')->on('outputs')->onDelete('cascade');
        }, 'ENGINE=InnoDB');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}