<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outputs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('subdepartment_id')->nullable();
            $table->string('name');
            $table->integer('percentage');
            $table->string('location');
            $table->string('challange')->nullable();
            $table->string('follow_up')->nullable();
            $table->string('link_report')->nullable();
            $table->string('link_photo_1')->nullable();
            $table->string('link_photo_2')->nullable();
            $table->string('link_photo_3')->nullable();
            $table->string('link_photo_4')->nullable();
            $table->string('desc_photo_1')->nullable();
            $table->string('desc_photo_2')->nullable();
            $table->string('desc_photo_3')->nullable();
            $table->string('desc_photo_4')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->timestamps();

            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        }, 'ENGINE=InnoDB');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outputs');
    }
}
