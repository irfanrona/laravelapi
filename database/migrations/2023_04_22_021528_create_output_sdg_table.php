<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutputSdgTable extends Migration
{
    public function up()
    {
        Schema::create('output_sdg', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('output_id');
            $table->unsignedBigInteger('sdg_id');
            $table->timestamps();
            
            $table->foreign('output_id')->references('id')->on('outputs')->onDelete('cascade');
            $table->foreign('sdg_id')->references('id')->on('sdgs')->onDelete('cascade');
        }, 'ENGINE=InnoDB');
    }

    public function down()
    {
        Schema::dropIfExists('output_sdg');
    }
}
